# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# include <stdbool.h>



void choix_fonction(char* choix[10]) {
		

	if (strcmp(choix[0],"show-airports") == 0 
	   && choix[1] != NULL
 	   && choix[2] == NULL 
	   && strlen(choix[1]) == 2 ) {
		
		printf("%s %s \n", choix[0], choix[1]);
		return ;
	}
	
	if (strcmp(choix[0],"show-airlines") == 0 
	   && choix[1] != NULL
	   && choix[2] == NULL
	   && strlen(choix[1]) ==3 ) {
		
		printf("%s %s \n", choix[0], choix[1]);
		return ;
	}

	// show flights a faire


	if (strcmp(choix[0],"show-flights") == 0 
	   && choix[1] != NULL
	   && choix[2] != NULL 
	   && choix[3] != NULL 
	   && choix[4] == NULL
	   && strlen(choix[1]) == 3 
	   && strlen(choix[2]) <= 2 
	   && strlen(choix[3]) <= 2  ) {
		printf("%s %s %s %s\n",choix[0],choix[1],choix[2],choix[3]);
		return;
	}


	if (strcmp(choix[0],"most-delayed-flights")==0 
	   && choix[1] == NULL ) {
		
		printf("%s \n", choix[0]);
		return ;
	}

	if (strcmp(choix[0],"most-delayed-airlines")==0 
	   && choix[1] == NULL ) {
		
		printf("%s\n", choix[0]);
		return ;
	}

	if (strcmp(choix[0],"delayed-airline")==0 
	   && choix[1] != NULL
	   && choix[2] == NULL
	   && strlen(choix[1]) == 2 ) {
		
		printf("%s %s \n", choix[0], choix[1]);
		return ;
	}


	// most delayed airline a faire


	if (strcmp(choix[0],"changed-flights")==0 
	   && choix[1] != NULL
	   && choix[2] != NULL
	   && choix[3] == NULL 
	   && strlen(choix[1]) <= 2 
	   && strlen(choix[2]) <= 2 ){ 
		printf("%s %s %s\n", choix[0], choix[1], choix[2]);
		return ;
	}

	 
	if (  strcmp(choix[0],"avg-flight-duration")==0 
   	   && choix[1]!=NULL
	   && choix[2]!=NULL 
	   && choix[3]==NULL
	   && strlen(choix[1]) == 3 
	   && strlen(choix[2]) == 3 ) {
		
		printf("%s %s %s\n", choix[0], choix[1],choix[2]);
		return ;
	}
	

	if (  strcmp(choix[0],"find-itinerary")==0 
   	   && choix[1]!=NULL
	   && choix[2]!=NULL 
	   && choix[3]!=NULL
	   && choix[4]!=NULL
	   && choix[5]==NULL
	   && strlen(choix[1]) == 3 
	   && strlen(choix[2]) == 3 
	   && strlen(choix[3]) <= 2 
	   && strlen(choix[4]) <= 2 ) {
		
		printf("%s %s %s %s %s\n", choix[0]
					 , choix[1]
					 , choix[2]
					 , choix[3]
					 , choix[4] );
		return ;
	}

	if (  strcmp(choix[0],"find-multiplicity-itinerary")==0 
   	   && choix[1]!=NULL
	   && choix[2]!=NULL 
	   && choix[3]!=NULL
	   && choix[4]!=NULL
	   && choix[5]!=NULL
	   && choix[6]!=NULL
	   && choix[7]!=NULL 
	   && choix[8]==NULL ) {
		
		printf("%s %s %s %s %s\n", choix[0]
					 , choix[1]
					 , choix[2]
					 , choix[3]
					 , choix[4] );
		return ;
	}



	if (strcmp(choix[0],"quit")==0 && choix[1]==NULL) {
	       	printf("choix quit");
		return;
	}

	return ;
	

}







void lecture_fichier_commande () {
	
	// initialisation des constante
	char* line = NULL;
    	size_t line_buf_size = 0;

    	ssize_t nombre;
    	int nb_ligne=0;

	char* decoup[10];

	int i=0;

	while ((nombre=getline(&line, &line_buf_size, stdin))> -1) {

		i=0;		
		decoup[i]=strtok (line," \n\0");

		while (	decoup[i] != NULL) {
			i=i+1;
			decoup[i]=strtok (NULL," -\n");
		}
		decoup[i+1]=NULL;
		choix_fonction(decoup);
	}


}




int main() {	
	



	lecture_fichier_commande ();
  
    return 0;
}


