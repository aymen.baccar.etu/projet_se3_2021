
# include "../includes/structure.h"
# include "../includes/load.h"


void get_flights (char* line, flights** ptr_liste) {
        
    char* strToken;
    flights* cell_tmp=malloc(sizeof(flights));
    
    char* Token[13];
    int i=0;
    
    for (i=0;i<14;i++) {
        strToken = strsep (&line,",\n");
        if (strlen(strToken)!=0) Token[i]=strToken;
        else Token[i]="-1";
    }
    
    //staquage des données dans la structures.

    cell_tmp->date_flight.month=atoi(Token[0]);
    cell_tmp->date_flight.day=atoi(Token[1]);
    cell_tmp->date_flight.weekday=atoi(Token[2]);
    strcpy(cell_tmp->airline, Token[3]);
    strcpy(cell_tmp->ORG_AIR, Token[4]);
    strcpy(cell_tmp->DEST_AIR, Token[5]);
    cell_tmp->sched_dep = atoi (Token[6]);
    cell_tmp->dep_delay = atof(Token[7]);
    cell_tmp->airtime = atof(Token[8]);
    cell_tmp->DIST = atof(Token[9]);
	cell_tmp->sched_arr = atoi(Token[10]);
	cell_tmp->arr_delay = atof(Token[11]);
	cell_tmp->diverted = atoi(Token[12]);
    cell_tmp->cancelled = atoi(Token[13]);

    // ajout a la liste chainée

    cell_tmp->suivant=*ptr_liste;
    *ptr_liste=cell_tmp;
 
}


void get_airport (char* line, airports** ptr_liste) {
    
    // création des constantes.

    char* strToken;
    airports* cell_tmp=malloc(sizeof(airports));
    char* Token[7];
    int i=0;

    // stoquage des parties de la ligne découpés .
    for (i=0;i<7;i++) {
        strToken = strsep (&line,",\n");
        if (strlen(strToken)!=0) Token[i]=strToken;
        else Token[i]="200";
    }

    // Stoquage des données .

	strcpy(cell_tmp->id,Token[0]);
	strcpy(cell_tmp->airport,Token[1]);
	strcpy(cell_tmp->city,Token[2]);
	strcpy(cell_tmp->state,Token[3]);
    strcpy(cell_tmp->country,Token[4]);
    cell_tmp->latitude = atof(Token[5]);
	cell_tmp->logitude = atof(Token[6]);

    // ajout a la liste chainée
    
    cell_tmp->suivant=*ptr_liste;
    *ptr_liste=cell_tmp;
}


void get_airline (char* line, airline** ptr_liste) {

    char* strToken;
    airline* cell_tmp=malloc(sizeof( airline));
    
    strToken = strsep (&line , ",\n");
    if (strlen(strToken) !=0) strcpy(cell_tmp->id,strToken);
    else strcpy(cell_tmp->id,"NUL");

    strToken = strsep (&line , "\n");
    if (strlen(strToken) !=0) strcpy(cell_tmp->airline,strToken);
    else strcpy(cell_tmp->airline,"NUL");

    cell_tmp->suivant=*ptr_liste;
    *ptr_liste=cell_tmp;
}



int load_flights( void range_flights (char*,  flights** ) 
            , FILE *fp
            ,  flights** ptr_liste)   {

    char* line = NULL;
    size_t line_buf_size = 0;

    ssize_t nombre;
    int nb_ligne=0;

	getline(&line, &line_buf_size, fp);
	while ((nombre=getline(&line, &line_buf_size, fp))> -1) {
        nb_ligne=nb_ligne+1;
        range_flights(line,ptr_liste);
    }
    return nb_ligne;
    free(line);
}

int load_airport( void range_airport (char*,  airports** ) 
            , FILE *fp
            ,  airports** ptr_liste)   {

    char *line = NULL;
    size_t line_buf_size = 0;

    ssize_t nombre;
    int nb_ligne=0;

	getline(&line, &line_buf_size, fp);
	while ((nombre=getline(&line, &line_buf_size, fp))> -1) {
        nb_ligne=nb_ligne+1;
        range_airport(line,ptr_liste);
    }
    return nb_ligne;
    free(line);
}

int load_airline( void range_airline (char*,  airline** ) 
                , FILE *fp
                ,  airline** ptr_liste)   {

    char *line = NULL;
    size_t line_buf_size = 0;

    ssize_t nombre;
    int nb_ligne=0;

	getline(&line, &line_buf_size, fp);

    while ((nombre=getline(&line, &line_buf_size, fp))> -1) {
        nb_ligne=nb_ligne+1;
        range_airline(line,ptr_liste);
    }
    return nb_ligne;
    free(line);
}
