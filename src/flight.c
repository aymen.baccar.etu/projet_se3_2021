
# include "../includes/structure.h"
# include "../includes/flight.h"


void affiche_le_vol (flights* vols) {

    // affichage de la date :
    printf("%d,", (vols->date_flight).month);
    printf("%d,", (vols->date_flight).day);
    printf("%d,", (vols->date_flight).weekday);

    // affichage autre caractéristique :
    printf("%s,", vols->airline);
    printf("%s,", vols->ORG_AIR);
    printf("%s,", vols->DEST_AIR);
    printf("%f,", vols->DIST);
    printf("%d,", vols->sched_dep);
    printf("%f,", vols->dep_delay);
    printf("%f,", vols->airtime);
    printf("%f,", vols->arr_delay);
    printf("%d,", vols->sched_arr);
    printf("%d,", vols->diverted);
    printf("%d,", vols->cancelled);

}


int comparer_date(struct date d1 , struct date d2) {
    
        if ( (d1.month==d2.month)&&(d1.day == d2.day))
            return 0;
        else if  (((d1.month ==d2.month)&&(d1.day>d2.day))||((d1.month>d2.month)))
			return 1;
        else 
            return -1 ;
    }


void show_flights ( char* port_id
                  , date d
                  , flights* vols
                  , int heurs_min    
                  , int max_affiche) 
{

	while ( vols!=NULL && max_affiche!=0 )
	{
		if ( (comparer_date(vols->date_flight,d)==0) && (strcmp(vols->ORG_AIR,port_id)==0) ) 
        {
            if (vols->sched_dep > heurs_min) 
            {    
                max_affiche=max_affiche-1;

                affiche_le_vol (vols);
                printf("\n");
            
            }
        }
		vols=vols->suivant;
	}
    printf("\n");
}

void most_delayed_flights(flights* f)
{ 
	int i = 0 ;
    float max=0;
    flights* tmp=f;
	flights* affiche=NULL; 

	for (i=0;i<Q4;i=i+1)
	{	
        max = 0;
	    tmp=f; 
		while (tmp->suivant !=NULL ) 
		{
			if(tmp->arr_delay > max){
                affiche=tmp;
				max=tmp->arr_delay;
            }
        tmp=tmp->suivant;     
        }
        affiche->arr_delay=0;
        affiche_le_vol (affiche);
        printf("\n");
    }
    printf("\n");
}

void changed_flights( date d,flights* vols )
{

	while(vols!=NULL){
		if((comparer_date(d,vols->date_flight)==0) && ((vols->cancelled ==true)||(vols->diverted==true))) 
        {
			affiche_le_vol(vols);
            printf("\n");
        }
	    vols=vols->suivant;
	}
}


void find_itenerary( char* port_id1
                   , char* port_id2
                   , int jour
                   , int mois
                   , flights* vols)
{

	flights* temp = vols;
	printf("debut\n-\n-\n");
	

	while (temp!=NULL){
		if( (strcmp(temp->ORG_AIR,port_id1)==0)&&((temp->date_flight).day==jour)&&((temp->date_flight).month==mois)){

			
			
			printf("\n");
			flights* temp1= vols;
			while(temp1!=NULL){
				if( (strcmp(temp1->ORG_AIR,temp->DEST_AIR)==0)&&(strcmp(temp1->DEST_AIR,port_id2)==0)&&((temp1->date_flight).month==mois)&&((temp1->date_flight).day==jour)&&(temp->sched_arr<=temp1->sched_dep)){
					affiche_le_vol(temp);
					affiche_le_vol(temp1);
					return;
				}
				temp1=temp1->suivant;
			}
		}

		temp=temp->suivant;
	}
}

 void find_multicity_itenerary( char* depart
                              , char* destination1
                              , char* destination2
                              , int jour1 
                              , int mois1
                              , int jour2
                              , int mois2
                              , flights* vols)
{
	flights* temp=vols;
	
	while(temp!=NULL){
		if((strcmp(temp->ORG_AIR,depart)==0)&&((temp->date_flight).day==jour1)&&((temp->date_flight).month==mois1)){			
			printf("premier iténéraire: \n");
			affiche_le_vol(temp);
			flights* temp1=vols;
			while (temp1!=NULL){
				if( (strcmp(temp1->ORG_AIR,temp->DEST_AIR)==0)&&(strcmp(temp1->DEST_AIR,destination1)==0)&&((temp1->date_flight).day==jour1)&&((temp1->date_flight).month==mois1)&&(temp->sched_arr<=temp1->sched_dep)){
					printf("deuxième  iténéraire et arrivée à la 1ère destination  : \n");
					affiche_le_vol(temp1);
					printf("\n");
					flights* temp2=vols;
					while(temp2!=NULL){
						if( (strcmp(temp2->ORG_AIR,destination1)==0)&&((temp2->date_flight).day==jour2)&&((temp2->date_flight).month==mois2)&&(temp1->sched_arr<=temp2->sched_dep)){
							printf("3ème iténéraire: \n");
							affiche_le_vol(temp2);
							flights* temp3=vols;
							while(temp3!=NULL){
								if( (strcmp(temp3->ORG_AIR,temp2->DEST_AIR)==0)&&(strcmp(temp3->DEST_AIR,destination2)==0)&&((temp3->date_flight).day==jour2)&&((temp3->date_flight).month==mois2)&&(temp2->sched_arr<=temp3->sched_dep)){
										printf("\ndernier iténéraire: \n");
										affiche_le_vol(temp3);
										}
								else	
									temp3=temp3->suivant;
							}
						}
						else
							temp2=temp2->suivant;
					}
				}
				
			else
			
				temp1=temp1->suivant;
			}
		}


		else
			temp=temp->suivant;
		}
 }

void quit(airline** companies, airports** Aport, flights** vols) 
{
    airline* tmp0=NULL;
    airports* tmp1=NULL;
    flights* tmp2=NULL;
    

    while (*companies !=NULL ) 
    {
        tmp0=(*companies);
        (*companies)= (*companies)->suivant;
        free(tmp0);
    }

    while (*Aport !=NULL ) 
    {
        tmp1=(*Aport);
        (*Aport)= (*Aport)->suivant;
        free(tmp1);
    }
    
    while (*vols !=NULL ) 
    {
        tmp2=(*vols);
        (*vols)= (*vols)->suivant;
        free(tmp2);
    }
}
