# include "../includes/structure.h"
# include "../includes/load.h"
# include "../includes/companie.h"
# include "../includes/aeroport.h"
# include "../includes/flight.h"



void choix_fonction(char* choix[10]
                   , flights* vols
                   , airports* airport
                   , airline* companies
                   , int nb_comp
                   , int nb_Airport ) {


    date d;


	if (strcmp(choix[0],"show-airports") == 0 
	   && choix[1] != NULL
 	   && choix[2] == NULL 
	   && strlen(choix[1]) == 2 ) {
	
        show_airports ( choix[1], airport, vols, nb_Airport);

		return ;
	}
	
	if (strcmp(choix[0],"show-airlines") == 0 
	   && choix[1] != NULL
	   && choix[2] == NULL
	   && strlen(choix[1]) ==3 ) {
	    
        show_airlines(choix[1], companies, vols, nb_comp);
		return ;
	}



	if (strcmp(choix[0],"show-flights") == 0 
	   && choix[1] != NULL
	   && choix[2] != NULL 
	   && choix[3] != NULL 
	   && choix[4] == NULL
	   && strlen(choix[1]) == 3 
	   && strlen(choix[2]) <= 2 
	   && strlen(choix[3]) <= 2  ) {
		
        d.month=atoi(choix[2]);
        d.day=atoi(choix[3]);
        show_flights ( choix[1], d, vols, 0, -1);
		return;
	}


	if (strcmp(choix[0],"most-delayed-flights")==0 
	   && choix[1] == NULL ) {
		most_delayed_flights(vols);
		return ;
	}

	if (strcmp(choix[0],"most-delayed-airlines")==0 
	   && choix[1] == NULL ) {
		most_delayed_airline( vols,companies,nb_comp );
        return ;
	}
	
    if (strcmp(choix[0],"delayed-airline")==0 
	   && choix[1] != NULL
	   && choix[2] == NULL
	   && strlen(choix[1]) == 2 ) {
		delayed_airline(choix[1], vols, companies);
		return ;
	}
    if (strcmp(choix[0],"most-delayed-airlines-at-airport")==0 
	   && choix[1] != NULL
	   && choix[2] == NULL
	   && strlen(choix[1]) == 3 ) {
		most_delayed_ailines_at_airport(choix[1],nb_comp, vols, companies);
    }


	if (strcmp(choix[0],"changed-flights")==0 
	   && choix[1] != NULL
	   && choix[2] != NULL
	   && choix[3] == NULL 
	   && strlen(choix[1]) <= 2 
	   && strlen(choix[2]) <= 2 ){ 
	
        d.month=atoi(choix[1]);
        d.day=atoi(choix[2]);
        d.weekday=0;

        changed_flights( d, vols );
		return ;
	}

	 
	if (  strcmp(choix[0],"avg-flight-duration")==0 
   	   && choix[1]!=NULL
	   && choix[2]!=NULL 
	   && choix[3]==NULL
	   && strlen(choix[1]) == 3 
	   && strlen(choix[2]) == 3 ) {
		
        avg_flight_duration(choix[1], choix[2], vols);
		return ;
	}

	if (  strcmp(choix[0],"find-itinerary")==0 
   	   && choix[1]!=NULL
	   && choix[2]!=NULL 
	   && choix[3]!=NULL
	   && choix[4]!=NULL
	   && choix[5]==NULL
	   && strlen(choix[1]) == 3 
	   && strlen(choix[2]) == 3 
	   && strlen(choix[3]) <= 2 
	   && strlen(choix[4]) <= 2 ) {
		
	    find_itenerary (choix[1],choix[2],atoi(choix[3]),atoi(choix[4]),vols);
		return ;
	}

	if (  strcmp(choix[0],"find-multiplicity-itinerary")==0 
   	   && choix[1]!=NULL
	   && choix[2]!=NULL 
	   && choix[3]!=NULL
	   && choix[4]!=NULL
	   && choix[5]!=NULL
	   && choix[6]!=NULL
	   && choix[7]!=NULL 
	   && choix[8]==NULL ) {
		
		find_multicity_itenerary (choix[1],choix[2],choix[3],atoi(choix[4]),atoi(choix[5]),atoi(choix[6]),atoi(choix[7]),vols);
		return ;
	}



	if (strcmp(choix[0],"quit")==0 && choix[1]==NULL) {
	       	quit(&companies , &airport , &vols);
            
		exit(EXIT_SUCCESS);
	}

	return ;
	

}

void lecture_fichier_commande (flights* vols, airports* Aiport, airline* companies, int nb_comp, int nb_Airport ) {


	// initialisation des constante
	char* line = NULL;
    	size_t line_buf_size = 0;

    	ssize_t nombre;
    	int nb_ligne=0;

	char* decoup[10];

	int i=0;

	while ((nombre=getline(&line, &line_buf_size, stdin))> -1) {

		i=0;		
		decoup[i]=strtok (line," \n\0");

		while (	decoup[i] != NULL) {
			i=i+1;
			decoup[i]=strtok (NULL," -\n");
		}
		decoup[i+1]=NULL;
		choix_fonction(decoup, vols, Aiport, companies, nb_comp, nb_Airport );


	}

}


int main () {
    
    int nb_comp=0;
    int nb_Airport=0;

    struct flights* vols=NULL;
    struct airports* airport=NULL;
    struct airline* companies=NULL;

    FILE* fp = fopen("data/flights.csv", "r");
    FILE* fp1= fopen("data/airports.csv","r"); 
    FILE* fp2= fopen("data/airlines.csv","r");
    
    load_flights( get_flights ,fp, &vols);
    nb_Airport=load_airport( get_airport ,fp1, &airport);
    nb_comp=load_airline( get_airline ,fp2, &companies);

    lecture_fichier_commande (vols , airport, companies, nb_comp, nb_Airport);

    fclose (fp);
    fclose(fp1);
    fclose(fp2);

    return 0;
}

