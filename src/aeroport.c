
# include "../includes/structure.h"
# include "../includes/aeroport.h"


void affiche_le_aeroport (airports* Aport) {

    printf("%s,",Aport->id);
    printf("%s,",Aport->airport);
    printf("%s,",Aport->city);
    printf("%s,",Aport->state);
    printf("%s,",Aport->country);
    printf("%f,",Aport->logitude);
    printf("%f",Aport->latitude);
}

airports* recherche_airport ( airports* Aport
		                 , char* vols_airport
                         , int* compteur
                         , int parcourt_max) 
{
	*compteur=0;
	//parcour de la liste chainé.
        while (Aport!=NULL && strcmp(Aport->id,vols_airport)!=0 && parcourt_max != 0 ) 
	{ 
            parcourt_max=parcourt_max-1;
            Aport = Aport->suivant;
            (*compteur) = (*compteur) + 1; // incrémentation du copteur
        }
	return Aport;
}



void show_airports ( char* air, airports* Aport, flights* vols, int nb_port) {
	
    int i=0;
    int* Tableaux=malloc(nb_port*sizeof(int));
    int compteur=0;
	airports* tmp=NULL;

	while (vols!=NULL) {
        
        if ( strcmp(vols->airline,air)==0) {
            
            recherche_airport ( Aport, vols->ORG_AIR, &compteur,-1);
 	        Tableaux[compteur]=1;    

            recherche_airport ( Aport, vols->DEST_AIR, &compteur,-1);
            Tableaux[compteur]=1;
        }
        vols=vols->suivant;
    }

    for(i=0; i<nb_port;i=i+1) 
    {
        if (Tableaux[i]==1) {
            tmp=recherche_airport ( Aport, "", &compteur,i);
            affiche_le_aeroport (tmp);
            printf("\n");
        }
    }
    free(Tableaux);
    printf("\n");
}


void avg_flight_duration(char* id1,char* id2,flights* vols){
	float duree_moyenne=0;
	int i=0;
	while(vols!=NULL){
        
        if (vols->cancelled==0 && vols->diverted==0) {
			if((strcmp(id1,vols->ORG_AIR)==0) && (strcmp(id2,vols->DEST_AIR)==0))
            {
				i++;
				duree_moyenne+=vols->airtime;
			}

            if ((strcmp(id2,vols->ORG_AIR)==0)&&(strcmp(id1,vols->DEST_AIR)==0))
            {
        	    i++;
				duree_moyenne+=vols->airtime;
			}
        }
		vols=vols->suivant;
	}
    printf("average: %f ",duree_moyenne/i);
	printf(" (%d flights)\n",i);
    printf("\n");
}

