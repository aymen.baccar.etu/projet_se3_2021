
# include "../includes/structure.h"
# include "../includes/affiche.h"


void affiche_le_vol (flights* vol) {

	// Partie affichage de la date :
    printf("%d,", (vol->date_flight).month);
    printf("%d,", (vol->date_flight).day);
    printf("%d,", (vol->date_flight).weekday);
    	// affichage des id
    printf("%s,", vol->airline);
    printf("%s,", vol->ORG_AIR);
    printf("%s,", vol->DEST_AIR);
    	// affichage donnée liée aux vol
    printf("%d,",vol->sched_dep);
    printf("%d,",vol->sched_dep);
    printf("%f,",vol->dep_delay);
    printf("%f,",vol->airtime);
    printf("%f,",vol->arr_delay);
    printf("%d,",vol->sched_arr);
    printf("%d,",vol->diverted);
    printf("%d",vol->cancelled);
    
}


void affiche_le_aeroport (airports* Aport) {

    printf("%s,",Aport->id);
    printf("%s,",Aport->airport);
    printf("%s,",Aport->city);
    printf("%s,",Aport->state);
    printf("%s,",Aport->country);
    printf("%f,",Aport->logitude);
    printf("%f",Aport->latitude);
}


void affiche_le_airline (airline* Air) {

    printf("%s,",Air->id);
    printf("%s",Air->airline);
}

