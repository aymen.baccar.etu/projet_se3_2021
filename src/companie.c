# include "../includes/structure.h"
# include "../includes/companie.h"

// Commande lié a l'affichage des companie


void affiche_le_airline (airline* Air) 
{
    printf("%s,",Air->id);
    printf("%s",Air->airline);
}


void init_memoire (float** ptr_T,int longueur) {

    int i=0;
    for (i=0;i<longueur;i=i+1)  (*ptr_T)[i]=0;
    return ;
}


airline* recherche_comp ( airline* companie
		                 , char* vols_airline
                         , int* compteur
                         , int parcourt_max) 
{
	*compteur=0;
	//parcour de la liste chainé.
        while (companie!=NULL && strcmp(companie->id,vols_airline)!=0 && parcourt_max != 0 ) 
	{ 
            parcourt_max=parcourt_max-1;
            companie = companie->suivant;
            (*compteur) = (*compteur) + 1; // incrémentation du copteur
        }
	return companie;
}


void recherche_max ( float* l_retard
		           , int nb_comp
		           , float* max
		           , int* argmax) 
{ 	
    *max=0; // Variable stoquant la valeur maximal
    *argmax=0; // Variable stoquant le rend de la variable maximal.
    int j=0;

    for ( j=0 ; j<nb_comp ; j=j+1 )  
    {
        if ( (*max) < l_retard[j] ) 
	{
            (*max) =  l_retard[j]; // Affectation du max temporaire
            (*argmax) = j; // Affectation du rang i du max.
        }
    }
}


void show_airlines(char* port_id
                  , airline* companies 
                  , flights* vols 
                  , int nb_comp ) 
{
    int compteur=0;
    int i=0;
    float* Tableaux=malloc(nb_comp*sizeof(float));
	airline* tmp=NULL;

    // les case a 0 sont les compine partant pas de cett aeroport.
	while (vols != NULL) 
    {    
        if (strcmp(vols->ORG_AIR,port_id) == 0) 
        {
            recherche_comp ( companies , vols->airline , & compteur , -1);
            Tableaux[compteur]=Tableaux[compteur]+1;
        }
        vols=vols->suivant;
    }

    // affiche des aeroport lié aux case non nulle.
    for (i=0 ; i<nb_comp ; i=i+1) 
    {
        if  ( Tableaux[compteur]>0 ) 
        {   
            tmp=recherche_comp ( companies , "" , &compteur , i);
            affiche_le_airline (tmp);  
            printf("\n");
        }
    }
    free(Tableaux);
    printf("\n");
}


void delayed_airline(char* air , flights* vols, airline* companies)
{	
    float nb_retard=0;
    float nb_vol=0;
    int compteur=0;
	
    while ( vols != NULL) 
    {   
        if ( strcmp(vols->airline,air)==0 && vols->cancelled==0 && vols->diverted==0 ) { 
            if (vols->arr_delay>0) 
            {
                nb_retard=nb_retard+vols->arr_delay;
            }
            nb_vol=nb_vol+1; 
        }
        vols=vols->suivant;
    }        
    companies=recherche_comp ( companies , air , & compteur , -1 );
    affiche_le_airline (companies);
    printf(",%f\n\n", nb_retard/nb_vol);
}


void most_delayed_airline( flights* vols
                         , airline* companies
                         , int nb_comp ) {

	airline* tmp = companies; 
    
    float* nb_retard= malloc(nb_comp*sizeof(float));
    float* val_retard=malloc(nb_comp*sizeof(float));

    int i=0;
    int compteur=0;
    float max=0;
    int argmax=0;

    // Initialisation de la mémoire .

    init_memoire (&nb_retard,nb_comp);
    init_memoire (&val_retard,nb_comp);
    
    // Parcont de la liste et somme des retards.
	while(vols!=NULL)   
    {    
        recherche_comp (companies, vols->airline , &compteur , -1);
        if ( vols->cancelled==0 && vols->diverted==0 ) 
        {
            if ( vols->arr_delay>0 ) val_retard[compteur]=val_retard[compteur]+vols->arr_delay; 
            nb_retard[compteur]=nb_retard[compteur]+1;
        }
		vols=vols->suivant;
	}

    for (i=0;i<nb_comp;i=i+1) val_retard[i]=val_retard[i]/nb_retard[i];

    for (i=0;i<Q5;i=i+1) 
    {    
        recherche_max ( val_retard , nb_comp , &max , &argmax );

        // Nous avons donc obtenu le max et argmax
        tmp=recherche_comp (companies, "", &compteur , argmax);

        val_retard[argmax]=0; // mise a 0 du nombre de retard dans le tableaux afin de trouvé le second élément max .
        affiche_le_airline (tmp); // affichage du résultat .
    	printf(",%f\n",max);
    }

    // libération de l'espace mémoire aloué.

    free(val_retard);
    free(nb_retard);
    printf("\n");
}


void most_delayed_ailines_at_airport( char* nom
                                    , int nb_comp
                                    , flights* vols
                                    , airline* companies) {
    
    // Les variables servants le compteur.
    int compteur=0; // Variable permettant connaitre position de l'élément dans la liste chainée.
    int i=0;

    // tableaux de stoquages de donné tempon.
    float* tableaux = malloc(nb_comp*sizeof(float));

    // Les variables permettants d'obtenir le max.    
    float max=0;
    int argmax=0;
    
    // Variables pointeurs.
    struct airline* tmp=NULL;

    // Initialisation de la mémoire .
    for(i=0;i<nb_comp;i=i+1) tableaux[i]=0;

    while (vols !=NULL) { // Parcours de flights.csv
        compteur=0; // mise a 0 compteur avant de rechercher élément dans airline.csv .

        if (strcmp(vols->DEST_AIR,nom)==0 && vols->arr_delay>0 && vols->diverted==0 && vols->cancelled==0 ) 
        { // Recherche du bon AIRPORT, avec airline delai>0            
            recherche_comp (companies, vols->airline , &compteur , -1);
            tableaux[compteur]=tableaux[compteur]+1; // Incrementation du tableaux
        }
        vols=vols->suivant; // On avance dans flights.csv .
    }

    //fin partie test.
    for (i=0;i<Q7;i=i+1) {

        recherche_max ( tableaux , nb_comp , &max , &argmax );
        tmp=recherche_comp (companies, "", &compteur , argmax);

        tableaux[argmax]=0; // mise a 0 du nombre de retard dans le tableaux afin de trouvé le second élément max .
        
        affiche_le_airline (tmp); // affichage du résultat .
    	printf(",%f\n",max);

    
    }
    printf("\n");
    free(tableaux); // libération du tableaux afin de ne pas avoir de fuite mémoire .
}

