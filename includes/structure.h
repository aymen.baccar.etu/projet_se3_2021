#ifndef __STRUCTURE_H__
#define __STRUCTURE_H__


#include <stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>


#define MAXSIZE 20
#define Q4 5
#define Q5 5
#define Q7 3



typedef struct date {
	int month;
	int day;
	int weekday;
}date;


typedef struct flights{
	struct date date_flight;
	char airline[3];
	char ORG_AIR[4];
	char DEST_AIR[4];
	float DIST;
	int sched_dep;
	float dep_delay;
	float airtime;
	float arr_delay;
	int sched_arr;
	bool diverted;
	bool cancelled;
    struct flights* suivant;
	}flights;


typedef struct airports{
	char id[4];
	char airport[50];
	char city[50];
	char state[50];
	char country[50];
	float logitude;
	float latitude;
    struct airports* suivant;
}airports;


typedef struct airline {
    char id[3];
    char airline[30];
    struct airline* suivant;
}airline;

#endif
