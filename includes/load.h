#ifndef __LOAD_H__
#define __LOAD_H__


void get_flights (char* , struct flights** );

void get_airport (char* , struct airports** );

void get_airline (char* , struct airline** );

int load_flights ( void range_flights(char*,struct flights**),FILE* ,struct flights** );

int load_airport ( void range_airport(char*,struct airports**),FILE* ,struct airports** );

int load_airline ( void range_flights(char*,struct airline**),FILE* ,struct airline** );

#endif
