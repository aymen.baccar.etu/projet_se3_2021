#ifndef __FLIGHT_H__
#define __FLIGHT_H__

void affiche_le_vol ( flights* );

int comparer_date( date , date );

void show_flights( char* , date , flights* , int , int );

void most_delayed_flights (flights*);

void changed_flights ( date , flights* );

void find_itenerary( char* , char* , int , int , flights* );

void find_multicity_itenerary( char* , char* , char* , int , int , int , int , flights* );

void quit(airline** , airports** , flights** ); 

#endif
