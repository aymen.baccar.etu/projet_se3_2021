#ifndef __AEROPORT_H__
#define __AEROPORT_H__

void affiche_le_aeroport (airports*);

airports* recherche_airport ( airports*, char*, int* ,  int );

void show_airports ( char* , airports* , flights* , int ); 

void avg_flight_duration( char* , char* , flights* );

#endif
