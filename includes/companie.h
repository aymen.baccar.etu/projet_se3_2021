#ifndef __AFFICHE_H__
#define __AFFICHE_H__


airline* recherche_comp ( airline* , char* , int* , int ); 

void init_memoire ( float** , int );

void affiche_le_airline ( airline* );

void recherche_max ( float* , int , float* , int* ); 

void show_airlines ( char* , airline* , flights*, int );

void delayed_airline( char* , flights* , airline* );

void most_delayed_airline( flights* , airline* , int );

void most_delayed_ailines_at_airport( char* , int , flights* , airline* );

#endif
