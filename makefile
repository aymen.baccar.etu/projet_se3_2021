CC=gcc
CFFLAGS= -W -Wall -Wextra # warnings



main: main.o load.o companie.o aeroport.o flight.o
	gcc -o main flight.o aeroport.o companie.o load.o main.o $(CFFLAGS)

load.o : src/load.c includes/structure.h  includes/load.h
	$(CC) -c -o load.o src/load.c $(CFFLAGS)

companie.o: src/companie.c includes/structure.h includes/companie.h
	$(CC) -c -o companie.o src/companie.c $(CFFLAGS)

aeroport.o: src/aeroport.c includes/structure.h includes/aeroport.h
	$(CC) -c -o aeroport.o src/aeroport.c $(CCFLAGS)

flight.o: src/flight.c includes/structure.h includes/flight.h
	$(CC) -c -o flight.o src/flight.c $(CFFLAGS) 

main.o: src/main.c includes/load.h includes/companie.h includes/aeroport.h includes/flight.h
	$(CC) -c -o main.o src/main.c $(CFFLAGS)



clean: 
	rm -f *.o core

mrproper: clean
	rm -f main	
