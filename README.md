# Tutorat de Programmation Avancée (SE3 - 2020/2021)

Ce dépôt `GIT` contient le sujet et les données utiles pour la réalisation du
_petit_ projet du module de Programmation Avancée réalisé par Aymen Baccar et Loic Bal .


## Résumé

Le projet consiste à réaliser une application qui permet d'analyser 58592 vols aux États Unis en 2014.Nous avons chargé dans un premier temps des fichiers  CSV dans le but d’effectuer un certain nombre de requêtes que l’utilisateur pourra renseigner via le terminal .

L'objectif de ce projet est de maîtriser les compétences acquises durant les cours de programmation avancée : structures de données complexes, lecture / écriture de fichiers, compilation séparée et automatique, utilisation de gestionnaire de version..


## CONTENU
Ce dépot contien différent repertoires avec les fichier nécéssaire à la compilation et l'execution des taches demandés dans le cahier de charges.

- **data:**
    
    1. LICENSE   

    2. flights.csv : fichier contenant les informations sur 59592 vols 
    2. airlines.csv: fichier contenant les informations sur 13 compagnies aériennes.
    3. airports.csv: fichier contenant les informations sur 322 aéroports.

- **src:**
    1. affiche.c : affichage des données .
    2. delayed.c : requetes gérant les retards.
    3. load.c: chargement des données dans les listes chainées.
    4. tools.c: définition des structures de données.
    5. lecture.c : lecture de la commande de l'utilisateur.
    6. companie.c : les requete gérant les airlines : show_airlines ,delayed_ailines,most_delayed_airlines et most_delayed_ailines_at_airport
    7. flight.c : les requetes show_flights,most_delayed_flights,changed_flights,find_itenerary et find_multicity_itenerary
    5. main.c  : l'execution de la commande saisie par l'utilisateur

- **include:**
    -aeroport.h
    -flight.h
    -companie.h
    -load.h
    -structure.h

-** racine: ** 
    -makefile: gestion de la compilation automatique.
    -README.md: fichier récapitulatif qui sert de guide pour l'utilisateur.
    -rapport.pdf



## Makefile
Notre makefile permet de compiler chaque fichier .c et .h ,Le compilateur utilisé est gcc avec les options -g -W -Wall -Wextra 
La structure de notre makefile est la suivante :


clean : la commande associée supprime tous les fichiers intermédiaires.


mrproper :  permet une reconstruction complète du projet lors de l’appel suivant à make

## Guide d'utilisation:

Le programme éxecute les étapes suivantes d'une manière cycique:

1. charger le fichier de données
2. attendre une commande
3. traiter la commande
4. afficher le résultat de cette commande
5. revenir à l'étape 2


Les commandes seront les suivantes:

- `show-airports <airline_id>`  : affiche tous les aéroports depuis lesquels la compagnie aérienne `<airline_id>` opère des vols
- `show-airlines <port_id>`: affiche les compagnies aériens qui ont des vols qui partent de l'aéroport `<port_id>`
- `show-flights <port_id> <date> [<time>] [limit=<xx>]` : affiche les vols qui partent de l'aéroport à la date, avec optionnellement une heure de début, et limité à xx vols
- `most-delayed-flights`     : donne les 5 vols qui ont subis les plus longs retards à l'arrivée
- `most-delayed-airlines`    : donne les 5 compagnies aériennes qui ont, en moyenne, le plus de retards
- `delayed-airline <airline_id>`    : donne le retard moyen de la compagnie aérienne passée en paramètre
- `most-delayed-airlines-at-airport <airport_id>`    : donne les 3 compagnies aériennes avec le plus de retard d'arrivé à l'aéroport passée en paramètre
- `changed-flights <date>` : les vols annulés ou déviés à la date <date> (format M-D)
- `avg-flight-duration <port_id> <port_id>`: calcule le temps de vol moyen entre deux aéroports
- `find-itinerary <port_id> <port_id> <date> [<time>] [limit=<xx>]`: trouve un ou plusieurs itinéraires entre deux aéroports à une date donnée (l'heure est optionnel, requête peut être limité à `xx` propositions, il peut y avoir des escales)
- `find-multicity-itinerary <port_id_depart> <port_id_dest1> <date> [<time>] <port_id_dest2> <date> [<time>] ... <port_id_destN> <date> [<time>]`: trouve un itinéraire multiville qui permet de visiter plusieurs villes (il peut y avoir des escales pour chaque vol intermediaire)
- `quit`      : quit

L'utilisateur doit saisir une requête parmi ces 12: les 11 prmières requêtes servent à afficher les informations souhaitées et une saisie de **quit** permet la libération de la mémoire et la fin du programme. 






